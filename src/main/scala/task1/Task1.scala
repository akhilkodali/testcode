package task1

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}

case class Msg(numberOfHopsTravelled: Int)

class TaskActor(n: Int) extends Actor with ActorLogging {

  val current = self.path.name.toInt
  val forwardTo: Option[Int] = if (current < n) Some(current+1) else None

  lazy val forwardRef = forwardTo.map(i => context.actorSelection("../"+i))

  override def receive: Receive = {
    case m: Msg =>
      log.info(m.numberOfHopsTravelled.toString)
      forwardRef.foreach(_ ! m.copy(numberOfHopsTravelled = m.numberOfHopsTravelled + 1))
  }
}


object Task {

  def main(args: Array[String]): Unit = {
    require(args.length > 0, "please specify number of actors n")
    require(args.head.toInt > 0, "number of actors should be greater than 0")
    val n = if (args.length > 0) args.head.toInt else 5
    val system = ActorSystem()

    val firstRef = (1 to n).map(i => system.actorOf(Props(new TaskActor(n)), i.toString)).head
    firstRef ! Msg(0)
  }
}