import scala.util.Random

package object task4 {
  val r = new Random()
  val Low = 0
  val High = 100

  def random: Int = r.nextInt(High - Low) + Low
}
package task4 {

  import akka.actor._

  import scala.collection.mutable
  import scala.concurrent.duration._

  case class Trigger(time: Duration)
  case object Acknowledgement
  case object Start


  class Agent extends Actor with ActorLogging {
    var count = 0

    override def receive: Receive = {
      case Trigger(t) =>
        log.info(s"${self.path.name} $t")
        count += 1
        if (count < 10)
          sender ! Trigger(t + (random milliseconds))
        else
          count = 0

        sender ! Acknowledgement
    }
  }

  class Scheduler(agentRefs: Seq[ActorRef]) extends Actor with ActorLogging {

    val q = new mutable.PriorityQueue[(Duration, ActorRef)]()(Ordering.by(_._1))

    override def preStart(): Unit =
      agentRefs.foreach(r => q.enqueue(random.milliseconds -> r))


    override def receive: Receive = {
      case Acknowledgement | Start if q.nonEmpty =>
        val (time, sendTo) = q.dequeue()
        sendTo ! Trigger(time)

      case Trigger(t) =>
        q.enqueue(t -> sender)
        val (time, sendTo) = q.dequeue()
        sendTo ! Trigger(time)
    }

  }

  object Task {

    def main(args: Array[String]): Unit = {

      require(args.length > 0, "please specify number of actors n")
      require(args.head.toInt > 0, "number of actors should be greater than 0")

      val n = if (args.length > 0) args.head.toInt else 5
      val system = ActorSystem()

      val agents = (1 to n).map(i => system.actorOf(Props(new Agent()), i.toString))
      val scheduler = system.actorOf(Props(new Scheduler(agents)), "scheduler")
      scheduler ! Start

    }
  }

}

