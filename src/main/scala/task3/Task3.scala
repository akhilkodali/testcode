import akka.actor._

package object task3 {

  import scala.util.Random

  type Msg = String

  val r = new Random()
  val Low = 1
  val High = 100

  def random: Int = r.nextInt(High - Low) + Low


  class Task3Actor(n: Int) extends Actor with ActorLogging {

    val current = self.path.name.toInt

    // adjusting between 0 and 1 based indexing
    val prev = ((current - 2 + 2 * n) % n) + 1
    val next = (current % n) + 1

    var prevReceived = false
    var nextReceived = false

    def fwdRefSelection(i: Int) = context.actorSelection(s"../${i}")

    def scheduleSend(m: Msg, i: Int) = {
      import scala.concurrent.duration._
      import context.dispatcher

      context.system.scheduler.scheduleOnce(random milliseconds) {
        //log.info(self.toString())
        fwdRefSelection(i).tell(m, self)
      }
    }

    def senderI = sender().path.name.toInt

    override def receive: Receive = {
      // when the message is first sent from main
      case m: Msg if sender.path.name == "deadLetters" =>
        scheduleSend(m, prev)
        scheduleSend(m, next)

      case m: Msg if senderI == next =>
        scheduleSend(m, prev)
        context.become(printAndEnd(prev))

      case m: Msg if senderI == prev =>
        scheduleSend(m, next)
        context.become(printAndEnd(next))
    }

    def printAndEnd(await: Int): Receive = {
      case m if senderI == await =>
        log.info(s"$current $m")
    }

  }


}

package task3 {

  object Task {

    def main(args: Array[String]): Unit = {
      require(args.length > 0, "please specify number of actors n")
      require(args.head.toInt > 0, "number of actors should be greater than 0")
      //val n = if (args.length > 0) args.head.toInt else 5
      val n = args.head.toInt
      val system = ActorSystem()

      val rNum = r.nextInt(n - 1) + 1
      val listRef = (1 to n).map(i => system.actorOf(Props(new Task3Actor(n)), i.toString))
      listRef(rNum) ! s"hello from: $rNum"
    }

  }

}