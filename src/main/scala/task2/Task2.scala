package task2

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}

case class Msg(times: Seq[Long])

class Task2Actor(n: Int) extends Actor with ActorLogging {

  val current = self.path.name.toInt
  val forwardTo: Option[Int] = if (current < n) Some(current+1) else None

  lazy val forwardRef = forwardTo.map(i => context.actorSelection("../"+i))

  private def getA(i: Int) = if(i==n-1) "A" else "a"

  override def receive: Receive = {
    case m: Msg =>
      val fMsg = m.copy(times = m.times :+ System.currentTimeMillis())
      forwardRef map (
        _ ! fMsg
        ) getOrElse {
          for ((time, i) <- fMsg.times.zipWithIndex) {
            log.info(s"${getA(i)}ctor ${i + 1}, message received $time")
          }
      }
  }
}


object Task {

  def main(args: Array[String]): Unit = {
    require(args.length > 0, "please specify number of actors n")
    require(args.head.toInt > 0, "number of actors should be greater than 0")
    val n = if (args.length > 0) args.head.toInt else 5
    val system = ActorSystem()

    val firstRef = (1 to n).map(i => system.actorOf(Props(new Task2Actor(n)), i.toString)).head
    firstRef ! Msg(Nil)
  }
}